package model;

public class User {
    private String username;
    public void setUsername(String username) {
        setUsernamePrivate(username);
    }
    private void setUsernamePrivate(String username) {
        this.username = username;
        System.out.println("Private setter called");
    }
    @Deprecated
    public void deprecatedMethod(){
        System.out.println("DeprecatedMethod() called");
    }
}
