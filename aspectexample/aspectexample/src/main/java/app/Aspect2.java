/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;

@Aspect
public class Aspect2 {
    
    @Pointcut("execution(static public * app.Application.*(..))")
    public void p1() {}
    
    @Before("p1()")
    public void action() {
        System.out.println("Aspect2.action");
    }
}
