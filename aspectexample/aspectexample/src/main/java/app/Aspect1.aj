package app;

public aspect Aspect1 {

    pointcut p1(): execution(public * *Service.*(..));

    before():p1() {
       // System.out.println("Aspect1.before");
    }

    pointcut floatPointcut(): call(float *(..));
    before(): floatPointcut(){

        System.out.println("Metoda zwracajaca float.");
    }

    pointcut stringPointcut(): call(* *(String));
    before(): stringPointcut(){
      //  System.out.println("Metody z jednym parametrem typu String");
    }

    pointcut twoParametersPointcut(): call(* *(*,*));
    before(): twoParametersPointcut(){
        System.out.println("Metoda z dwoma dowolnymi parametrami.");
    }

    pointcut servicePointcut(): call(public * services.AppService.*(..));
    before(): servicePointcut(){
        System.out.println("Metoda publiczna w klasie *Service w pakiecie services.");
    }

    pointcut setPointcut(): call(!public * model.*.set*(..));
    before(): setPointcut(){
        System.out.println("Niepubliczne metody set* w klasach z pakietu model");
    }
    pointcut deprecatedPointcut(): call(@Deprecated * *(..));
    before(): deprecatedPointcut(){
        System.out.println("Metoda z adnotacja @Deprecated");
    }

    pointcut executionTime(): execution(public * services.*.*(..)) && !(execution(* set*(..)) || execution(* get*(..)));
    Object around(): executionTime(){
        long time = System.nanoTime();
        Object returnedObject = proceed();
        System.out.println("Czas wykonania: " + (System.nanoTime() - time)+" nanosecund");
        return returnedObject;
    }

    pointcut callToUnsafeCode() : execution(* model.*.set*(..)) && within(@Immutable *);;
    declare error : callToUnsafeCode()  : "Blad kompilacji dla metod z adnotacja @Immutable,!";

}
